# HISTORY

### Versioning
The Dotstat version of the NSIWS Docker image is built on top of the Eurostat [NSIWS image](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored).
In Gitlab and in the changelog the release version of the OECD specific NSIWS is indicated with the last digit in the version number of the image, e.g. v8.2.1.[X] is version 8.2.1 of the default NSIWS and [x] indicates the OECD specific release version.  

## v8.9.2
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.9.2 Changelog
The changelog for the base NSIWS v8.9.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.9.2/CHANGELOG.md)

### Issues 
- [#232](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/232) NSI Dissemination db health check 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/227) Corrected bugs in ref metadata implementation
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/222) SDMX-JSON: Hierarchy in Hierarchical Codelist (HCL) misses the ID (and links) - SDMXRI-1816
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/215) Fix failing JSON v1.0 unit tests in SdmxSource - SDMXRI-1812
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/212) SDMX-JSON V2 NSI accept header support - SDMXRI-1809
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/192) NSI data retriever metadata support - SDMXRI-1795 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/282) Metadata attributes mapping sets management - SDMXRI-1772 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/183) Wrong data returned when querying for several frequencies - SDMXRI-1768
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/167) LastNObservations parameter returns first N observations - SDMXRI-1757
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/150) SDMX-CSV 2.0.0 (meta)data download - SDMXRI-1806
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/233 SDMX-JSON 2.0.0 (meta)data download - SDMXRI-1779
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Performance not acceptable when retrieving the first observation per seriesKey in a large dataflow - SDMXRI-1758


## v8.8.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.8.0 Changelog
The changelog for the base NSIWS v8.8.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.8.0/CHANGELOG.md)

### Issues 
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/165) Implement SDMX-CSV 2.0.0 data reader in SdmxSource - SDMXRI-1745 - NSI WS 8.8.0
- [#188](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/188) Error when upgrading MSDB from v6.14 to v6.17 - SDMXRI-1764
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/143) AfterPeriod and BeforePeriod in CCs must not be converted to StartPeriod and EndPeriod - SDMXRI-1687 - NS WS 8.8.0
- [#156](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/156) Inconsistent structure ID in structure-specific data messages - SDMXRI-1732 - NSI WS 8.8.0
- [#162](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/162) Wrong class name in URN when retrieving stub artefacts - SDMXRI-1748 - NSI WS 8.8.0

## v8.7.1
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.7.1 Changelog
The changelog for the base NSIWS v8.7.1 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.7.1/CHANGELOG.md)

### Issues 
- [#171](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/171) Fix of regression bug for usage of reserved keyword in SQL queries
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) Deploy NSI version 8.7.1 in DevOps
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/224) Allow retrieving data with constrained but non-provided optional attributes


## v8.5.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.5.0 Changelog
The changelog for the base NSIWS v8.5.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.5.0/CHANGELOG.md)

### Issues 
- [#21](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/21) Support for non-calendar year reporting
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/81) Implement SDMX-JSON writer for StructureSets, MSD, Metadataflow and ProvisionAgreement
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/95) Incorrect HTTP status code returned for failing data queries
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/108) For "available" ContentConstraint replace ReferencePeriod by CubeRegion-TimeRange
- [#117](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/117) Further performance improvements for range requests
- [#121](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/121) Change NSIWS response code to 404, when mappingsets are missing
- [#134](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/134) AnnotationTitle is still limited to 70 characters when not attached to a dataflow (and all SDMX objects by extension) 
- [#137](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/137) Improve the successful message when updating a non-final codelist
- [#146](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/146) Escape dimension column in sql query for dynamic actual constraint
- [#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/147) Unable to upload non-final MSD
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/157) NSI uses wrong period end of the filter's endPeriod parameter & high-frequency values are ignored
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) Deploy NSI version 8.5.0 in DevOps
- [#216](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/216) NSI WS should take data database connection parameters from configuration


## v8.2.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.2.0 Changelog
The changelog for the base NSIWS v8.2.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.2.0/CHANGELOG.md)
Please note that there is a typo in this change log and the actual MSDB version supported by NSIWS v8.2.0 is v6.14 (confirmed by ESTAT).

### Issues 
- [#73](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/73) StartDate of a ContentConstraintObjectCore instance can be changed to invalid value
- [#97](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/97) Treatments of afterPeriod and beforePeriod are mixed up in TimeRangeCore implementation
- [#119](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/119) Fix PIT related code in eurostat's maapi.net library
- [#136](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/136) Unclear error message for content constraint with missing referenced dataflow
- [#138](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/138) Make rest data retrievals asynchronous
- [#139](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/139) Deploy NSI version 8.2.0 in DevOps
- [#140](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/140) Improve performance when getting the Frequency dimension from a DSD

## v8.1.3
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and databases. 

### NSIWS v8.1.3 Changelog
The changelog for the base NSIWS v8.1.3 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.3/CHANGELOG.md)

### Issues 
- [#31](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/31) Incorrect sender ID in SDMX messages
- [#35](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/35) Improve error/status message for all structure updates
- [#98](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/98) Enhancement of content constraint structure message validation
- [#101](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/101) Not possible to update parent relations in a non-final codelist used in a DSD
- [#104](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/104) SdmxRegistryService URL configuration
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/105) Performance improvements for 0-0 range requests
- [#106](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/106) Fix issue of last N actually selecting first N periods
- [#107](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/107) Fix writing of NULLs in JSON data message
- [#109](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/109) Values of attributes at group attachment level not written in series node in sdmx-json
- [#112](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/112) Support for DSD without Time dimension (part 2 for appropriate SDMX-JSON export)
- [#129](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/129) Upgrade to NSI version 8.1.3

## v8.1.2.1 
### Description
The changelog for the base NSIWS v8.1.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.2/CHANGELOG.md).

### Issues 
- [#37](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/37)  Authorization enabled by default. 
- [#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Add step to trigger performance tests in dotstatsuite-quality-assurance project. 

## v8.1.2 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and databases. 

### NSIWS v8.1.2 Changelog
The changelog for the base NSIWS image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.2/CHANGELOG.md)

### Issues 
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/48) Remove the custom .Stat Suite NSI-plugin(Replace NSI-Plugin) 
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) Upgrade to NSI version 8.1.2 





