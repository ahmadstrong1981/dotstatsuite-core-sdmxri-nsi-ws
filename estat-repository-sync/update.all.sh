#!/bin/sh 
Echo "-> authdb.sql.mirrored"
git -C ./authdb.sql.mirrored fetch
git -C ./authdb.sql.mirrored push

Echo "-> maapi.net.mirrored"
git -C ./maapi.net.mirrored fetch
git -C ./maapi.net.mirrored push

Echo "-> authorization.net.mirrored"
git -C ./authorization.net.mirrored fetch
git -C ./authorization.net.mirrored push

Echo "-> nsiws.net.mirrored"
git -C ./nsiws.net.mirrored fetch
git -C ./nsiws.net.mirrored push

