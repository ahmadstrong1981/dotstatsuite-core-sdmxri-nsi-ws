#!/bin/bash
TARGET_DB_ALIAS=nsiws-struct-db
TARGET_VERSION=${MA_DB_VERSION:-6.19}
ALWAYS_RESET=${MA_ALWAYS_RESET:-N}
MY_INSERT_NEW_ITEM_SCHEME_VALUES=${INSERT_NEW_ITEM_SCHEME_VALUES:-false}
DATASPACE_ID=${mappingStore__Id__Default:-MappingstoreServer}

echo Target database: [$TARGET_DB_ALIAS]
echo MA Target version : [$TARGET_VERSION]
echo MA Always reset ..: [$ALWAYS_RESET]

echo SQL Server ....: [$SQL_SERVER]
echo SQL Database ..: [$SQL_DATABASE]
echo SQL User ......: [$SQL_USER]
if [ -z "$SQL_PASSWORD" ]; then
	echo SQL Password ..: NOT provided !!!
else
	echo SQL Password ..: Provided
fi
echo MA SQL User ......: [$MA_SQL_USER]
if [ -z "$MA_SQL_PASSWORD" ]; then
	echo MA SQL Password ..: NOT provided !!!
else
	echo MA SQL Password ..: Provided
fi
echo Sender Id .....: [$SENDER_ID]
echo Dataspace Id ..: [$DATASPACE_ID]
echo InsertNewItems.: [$MY_INSERT_NEW_ITEM_SCHEME_VALUES]

# Copy config template file and inject parameter values 
cp config-templates/maapi-tool.config maapi-tool/Estat.Sri.Mapping.Tool.dll.config

sed -i "s/#SQL_SERVER#/$SQL_SERVER/g" maapi-tool/Estat.Sri.Mapping.Tool.dll.config
sed -i "s/#SQL_DATABASE#/$SQL_DATABASE/g" maapi-tool/Estat.Sri.Mapping.Tool.dll.config
sed -i "s/#SQL_USER#/$MA_SQL_USER/g" maapi-tool/Estat.Sri.Mapping.Tool.dll.config
sed -i "s/#SQL_PASSWORD#/$MA_SQL_PASSWORD/g" maapi-tool/Estat.Sri.Mapping.Tool.dll.config

cp config-templates/nsi-ws.config config/app.config

sed -i "s/#SQL_SERVER#/$SQL_SERVER/g" config/app.config
sed -i "s/#SQL_DATABASE#/$SQL_DATABASE/g" config/app.config
sed -i "s/#SQL_USER#/$SQL_USER/g" config/app.config
sed -i "s/#SQL_PASSWORD#/$SQL_PASSWORD/g" config/app.config
sed -i "s/#SENDER_ID#/$SENDER_ID/g" config/app.config
sed -i "s/#DATASPACE_ID#/$DATASPACE_ID/g" config/app.config
sed -i "s/#INSERT_NEW_ITEM_SCHEME_VALUES#/$MY_INSERT_NEW_ITEM_SCHEME_VALUES/g" config/app.config

MA_ALIAS_STATUS=$(dotnet maapi-tool/Estat.Sri.Mapping.Tool.dll list | grep $TARGET_DB_ALIAS;)

if [ -z "$MA_ALIAS_STATUS" ]; then
	echo Alias $TARGET_DB_ALIAS not defined in configuration file
else
	echo Alias $TARGET_DB_ALIAS found in configuration file
	
	if echo "$MA_ALIAS_STATUS" | grep "Not a Mapping Store"; then
		echo $TARGET_DB_ALIAS is not a Mapping Store database - Attempting to initialize

		dotnet maapi-tool/Estat.Sri.Mapping.Tool.dll init -m $TARGET_DB_ALIAS -f 
	else
		if [ "$ALWAYS_RESET" == "Y" ] || [ "$ALWAYS_RESET" == "y" ]; then
			echo $TARGET_DB_ALIAS is already a Mapping Store database - Attempting to re-initialize

			dotnet maapi-tool/Estat.Sri.Mapping.Tool.dll init -m $TARGET_DB_ALIAS -f 			
		else
			echo $TARGET_DB_ALIAS is a Mapping Store database - Attempting to upgrade to v$TARGET_VERSION
			
			dotnet maapi-tool/Estat.Sri.Mapping.Tool.dll upgrade -m $TARGET_DB_ALIAS -t $TARGET_VERSION -f
		fi
	fi
	
	dotnet NSIWebServiceCore.dll
fi
